package cn.echcz.twitbase

import cn.echcz.twitbase.dao.TwitDao
import cn.echcz.twitbase.domain.Twit
import cn.echcz.twitbase.util.HbaseUtils

object TwitDaoTest {
  def main(args: Array[String]): Unit = {
    /*val connection = HbaseUtils.getConnection(Map("hbase.zookeeper.quorum" -> "anode"))
    val admin = HbaseUtils.getAdmin(connection)
    HbaseUtils.fastExe(connection, admin){
      HbaseUtils.createTable(admin, "twits", "info")
    }*/

    val connection = TwitDao.twitConn
    val table = TwitDao.twitTable
    HbaseUtils.fastExe(connection, table){
      println("----插入推文jack:hello hbase, jack:hi spark, jack: welcome hadoop----")
      TwitDao.put(Twit("jack", "hello hbase"))
      Thread.sleep(1000)
      TwitDao.put(Twit("jack", "hi spark"))
      Thread.sleep(1000)
      TwitDao.put(Twit("jack", "welcome hadoop"))
      println("----扫描jack的推文-----")
      val twits = TwitDao.scanByUserId("jack")
      twits.foreach(println(_))
    }

  }

}
