package cn.echcz.twitbase

import cn.echcz.twitbase.util.HbaseUtils

object CreateTableTest {
  def main(args: Array[String]): Unit = {
    val connection = HbaseUtils.getConnection(Map("hbase.zookeeper.quorum" -> "anode"))
    val admin = HbaseUtils.getAdmin(connection)
    HbaseUtils.fastExe(connection, admin){
      HbaseUtils.createTable(admin, "twitsx", "msg", "kkk")
    }
  }
}
