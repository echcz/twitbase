package cn.echcz.twitbase

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ConnectionFactory, Put}
import org.apache.hadoop.hbase.util.Bytes
object WriterTest {
  def main(args: Array[String]): Unit = {
    val config = new Configuration()
    config.set("hbase.zookeeper.quorum","anode")
    val connection = ConnectionFactory.createConnection(config)
    val table = connection.getTable(TableName.valueOf("users"))
    try {
      // 创建要添加的行user1
      val row = new Put(Bytes.toBytes("user1"))
      // 往行row里添加数据info:name=jack
      column2Row(row, "info", "name", "jack")
      // 往行row里添加数据info:email=jack@163.com
      column2Row(row, "info", "email", "jack@163.com")
      // 往行row里添加数据info:password=123456
      column2Row(row, "info", "password", "123456")
      // 提交行
      table.put(row)
    } finally {
      table.close()
      connection.close()
    }
  }
  def column2Row(row: Put, f: String, q: String, v: String): Unit ={
    row.addColumn(Bytes.toBytes(f), Bytes.toBytes(q), Bytes.toBytes(v))
  }
}
