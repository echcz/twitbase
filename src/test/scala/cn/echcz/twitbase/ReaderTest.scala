package cn.echcz.twitbase

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ConnectionFactory, Get, Result}
import org.apache.hadoop.hbase.util.Bytes

object ReaderTest {
  def main(args: Array[String]): Unit = {
    val config = new Configuration()
    config.set("hbase.zookeeper.quorum", "anode")
    val connection = ConnectionFactory.createConnection(config)
    val table = connection.getTable(TableName.valueOf("users"))
    try {
      // 创建要读取的行user1
      val row = new Get(Bytes.toBytes("user1"))
      // 设置读取行里的info:email列
      column2Row(row, "info", "email")
      // 从users中读取user1表中的info:email
      val result = table.get(row)
      // 读取结果中的info:email的值
      println(result2Value(result, "info", "email"))
    } finally {
      table.close()
      connection.close()
    }
  }

  def column2Row(row: Get, f: String, q: String): Unit = {
    row.addColumn(Bytes.toBytes(f), Bytes.toBytes(q))
  }

  def result2Value(result: Result, f: String, q: String) = {
    Bytes.toString(result.getValue(Bytes.toBytes(f), Bytes.toBytes(q)))
  }
}
