package cn.echcz.twitbase

import cn.echcz.twitbase.dao.UserDao
import cn.echcz.twitbase.domain.User
import cn.echcz.twitbase.util.HbaseUtils

object UserDaoTest {
  def main(args: Array[String]): Unit = {
    HbaseUtils.fastExe(UserDao.userConn, UserDao.userTable) {
      println("----添加tom----")
      UserDao.put(User("user2", "Tom", "tom@163.com", "000"))
      println("----打印tom---")
      println(UserDao.get("user2"))
      println("----修改tom的密码----")
      UserDao.put(User("user2", "Tom", "tom@163.com", "abc"))
      println("----打印修改后tom---")
      println(UserDao.get("user2"))
      println("----删除tom---")
      UserDao.delete("user2")
    }
  }

}
