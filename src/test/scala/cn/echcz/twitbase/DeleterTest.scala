package cn.echcz.twitbase

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ConnectionFactory, Delete}
import org.apache.hadoop.hbase.util.Bytes

object DeleterTest {
  def main(args: Array[String]): Unit = {
    implicit def string2ByteArray(s: String): Array[Byte] = {
      Bytes.toBytes(s)
    }

    val config = new Configuration()
    config.set("hbase.zookeeper.quorum", "anode")
    val connection = ConnectionFactory.createConnection(config)
    val table = connection.getTable(TableName.valueOf("users"))
    try {
      // 设置要删除的行
      val row = new Delete("user1")
      // 设置要删除的单元格的为user1:info:email的当前版本
      // 如果要删除所有版本，请使用addColumns
      row.addColumn("info", "email")
      // 删除
      table.delete(row)
    } finally {
      table.close()
      connection.close()
    }
  }
}
