package cn.echcz.twitbase.domain

import cn.echcz.twitbase.util.RowKeyUtils

case class Twit(id: Array[Byte], userId: String, timestamp: Long, msg: String)

object Twit{
  def apply(userId: String, timestamp: Long, msg: String): Twit = {
    apply(RowKeyUtils.createRowKey(userId, -1 * timestamp), userId, timestamp, msg)
  }

  def apply(userId: String, msg: String): Twit ={
    val timestamp = System.currentTimeMillis()
    apply(userId, timestamp, msg)
  }
}
