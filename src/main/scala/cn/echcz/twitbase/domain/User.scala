package cn.echcz.twitbase.domain

case class User(id: String, name: String, email: String, password: String)