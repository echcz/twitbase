package cn.echcz.twitbase.util

import org.apache.hadoop.hbase.util.Bytes

object Common {
  /**
    * 隐式转化函数，用于将String转化为Array[Byte]
    */
  implicit def string2Bytes(s: String): Array[Byte] = {
    Bytes.toBytes(s)
  }
}
