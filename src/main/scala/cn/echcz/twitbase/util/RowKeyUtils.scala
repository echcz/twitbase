package cn.echcz.twitbase.util

import org.apache.hadoop.hbase.util.{Bytes, MD5Hash}
import cn.echcz.twitbase.util.Common._


/**
  * rowKey工具类
  */
object RowKeyUtils {
  /**
    * 根据原始的key和时间戳创建定长(40字节)的rowkey，前部分是key的md5值，后半部分是时间值
    *
    * @param key       原始的key
    * @param timestamp 时间戳
    * @return 合成的定长rowkey
    */
  def createRowKey(key: String, timestamp: Long): Array[Byte] = {
    val result = new Array[Byte](40)
    var offset = 0
    offset = Bytes.putBytes(result, offset, MD5Hash.getMD5AsHex("jack"), 0, 32)
    Bytes.putBytes(result, offset, Bytes.toBytes(timestamp), 0, 8)
    result
  }

  /**
    * 根据原始的key和当前时间创建定长(40字节)的rowkey，前部分是key的md5值，后半部分是时间值
    *
    * @param key 原始key
    * @return 合成的定长rowkey
    */
  def createRowKey(key: String): Array[Byte] ={
    createRowKey(key, System.currentTimeMillis())
  }
  /*def main(args: Array[String]): Unit = {
    val time = System.currentTimeMillis()
    val bytes = string2Bytes(MD5Hash.getMD5AsHex("jack"))
    println(s"keyLen: ${bytes.length}")
    println(s"""key: ${bytes.mkString(" ")}""")
    val bytess = Bytes.toBytes(time)
    println(s"timeLen: ${bytess.length}")
    println(s"""time: ${bytess.mkString(" ")}""")
    val rowKey = createRowKey("jack", time)
    println(s"rowLen:${rowKey.length}")
    println(s"""row:${rowKey.mkString(" ")}""")
  }*/
}
