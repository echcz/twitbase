package cn.echcz.twitbase.exception

class RepeatException(msg: String) extends Exception(msg) {
  def this(){
    this("")
  }
}
