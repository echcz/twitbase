package cn.echcz.twitbase.dao

import cn.echcz.twitbase.domain.User
import cn.echcz.twitbase.util.HbaseUtils

/**
  * 用户Dao
  */
object UserDao {
  // 用户所在的表
  val TABLE = "users"
  // 用户拥有的列族
  val CF_INFO = "info"
  // 用户名
  val CQ_NAME = "name"
  // 用户邮箱
  val CQ_EMAIL = "email"
  // 用户密码
  val CQ_PASSWORD = "password"

  // 获取hbase连接，全局
  val userConn = HbaseUtils.getConnection(Map("hbase.zookeeper.quorum" -> "anode"))
  // 获取用户表，全局
  val userTable = HbaseUtils.getTable(userConn, TABLE)

  /**
    * 向hbase存入一个用户
    * @param user 用户
    */
  def put(user: User): Unit ={
    HbaseUtils.put(userTable, user.id, CF_INFO, CQ_NAME, user.name)
    HbaseUtils.put(userTable, user.id, CF_INFO, CQ_EMAIL, user.email)
    HbaseUtils.put(userTable, user.id, CF_INFO, CQ_PASSWORD, user.password)
  }

  /**
    * 从hbase获取一个用户，通过用户id
    * @param id 用户id
    * @return 用户
    */
  def get(id: String): User ={
    val name = HbaseUtils.get(userTable, id, CF_INFO, CQ_NAME)
    val email = HbaseUtils.get(userTable, id, CF_INFO, CQ_EMAIL)
    val password = HbaseUtils.get(userTable, id, CF_INFO, CQ_PASSWORD)
    User(id, name, email, password)
  }

  /**
    * 从hbase删除一个用户，通过用户id
    * @param id 用户id
    */
  def delete(id: String): Unit ={
    HbaseUtils.deleteRow(userTable, id)
  }
}
